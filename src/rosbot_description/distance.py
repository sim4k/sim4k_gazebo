#! /usr/bin/env python
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
import rospy
import numpy as np
# may require commands:
# pip3 install scikit-build opencv-python
from threading import Thread, Lock

import cv2


class Distance:
    def __init__(self):
        self.warning_distance = 0.5
        self.all_data = None
        # True - opencv window showing values
        self.vis = True
        self.init_vis_values()
        self.all_data = [0, 0, 0, 0]
        # range of clipping rgbd sensor ( full range = 640x480 )
        self.rgbd_size = [[i, j] for j in range(0,320) for i in range(100,540)]
        self.thread = Thread(target=self.main)
        self.thread.start()
        print("elo")
    # for visualization

    def init_vis_values(self):
        self.pts = np.array([[100,300], [100, 200],  
                        [200, 150], [300, 200],  
                        [300, 300]], 
                    np.int32) 
        self.pts = self.pts.reshape((-1, 1, 2)) 
        self.window_name = 'vis'
        self.orgs = [(200, 130),(80,250),(200,320),(320,250)] 

    def callback_lidar(self,data):
        assert isinstance(data, LaserScan)
        r = 30
        l = 719
        # including more points around for each direction
        self.all_data = [min(data.ranges[0:r] + data.ranges[l-r:l]), min(data.ranges[180-r:180+r]), min(data.ranges[360-r:360+r]),min(data.ranges[540-r:540+r])]


    def callback_pointcloud(self,data):
        assert isinstance(data, PointCloud2)
        gen = point_cloud2.read_points(data, field_names=("z"), skip_nans=True, uvs = self.rgbd_size)
        try:
            d = min(gen)[0]
            self.all_data[0] = d
        except:
            pass
        
        if(self.vis):
            self.visualize()

    def main(self):
        # rospy.init_node('sim4k_gazebo', anonymous=True)
        rospy.Subscriber('/camera/depth/points', PointCloud2, self.callback_pointcloud)
        rospy.Subscriber('/scan', LaserScan, self.callback_lidar)
        rospy.spin()

    def visualize(self):
        image = np.zeros((400,400,3), np.uint8)
        image = cv2.polylines(image, [self.pts], True, (255,255,255), 1) 

        for i,d in enumerate(self.all_data):
            if d <= self.warning_distance:
                color = (0,0,255)
            else:
                color = (0,255,0)
        
            image = cv2.putText(image, "%.3f" %(d)
            , self.orgs[i], cv2.FONT_HERSHEY_SIMPLEX ,  
                        0.5, color, 1, cv2.LINE_AA)

        cv2.imshow(self.window_name, image)  
        cv2.waitKey(1)

    def __del__(self):
        self.thread.join()
        cv2.destroyAllWindows()

