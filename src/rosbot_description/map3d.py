#! /usr/bin/env python
from sensor_msgs.msg import PointCloud2
from sensor_msgs import point_cloud2
from pathlib import Path
import os
import rospy
import numpy as np
import open3d as o3d
from geometry_msgs.msg import Twist, Point
from math import atan2
from nav_msgs.msg import Odometry
from scipy.spatial.transform import Rotation as R
import random
from imu import Yaw
import math 


class MainClass:
    def __init__(self):
        self.filename = 'your_path/sim4k_gazebo/Mapping/pointDataCloud'
        self.filename_load = 'your_path/sim4k_gazebo/Mapping/'
        self.how_many_pointclouds = 0
        self.frames = 25
        self.x = 0
        self.y = 0
        self.theta = 0
        self.imu = Yaw()

    def get_data(self, data):
        assert isinstance(data, PointCloud2)
        try:
            if self.how_many_pointclouds % self.frames == 0:
                point_cloud = list(point_cloud2.read_points(data, field_names=("x", "y", "z"), skip_nans=True))
                point_cloud = point_cloud[::5]
                mode = 'a' if os.path.exists(self.filename + str(self.how_many_pointclouds) + '.txt') else 'w'
                with open(self.filename + str(self.how_many_pointclouds) + '.txt', mode) as f:
                    for line in point_cloud:
                        f.write(
                            f"{str(line[0])[:10]} {str(line[1])[:10]} {str(line[2])[:10]} {str(self.theta)[:10]} {str(self.x)[:10]} {str(self.y)[:10]} \n")
            self.how_many_pointclouds += 1
        except Exception as e:
            print(e)

    def odom(self, msg):
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        self.theta = self.imu.get()

    # @staticmethod
    def prepare_data(self, data):
        rot = data[:, 3:4]
        global_x = data[:, 4:5]
        global_z = data[:, 5:6]
        data = data[:, :3]
        for i, line in enumerate(data):
            vect = [line[0], line[1], line[2]]
            vect = np.array(vect)
            matrixA = [[np.cos(rot[i]*np.pi/180), 0, np.sin(rot[i]*np.pi/180)],[0, 1, 0],[-np.sin(rot[i]*np.pi/180), 0, np.cos(rot[i]*np.pi/180)]]
            matrixB = [[np.cos(-90*np.pi/180), 0, np.sin(-90*np.pi/180)],[0, 1, 0],[-np.sin(-90*np.pi/180), 0, np.cos(-90*np.pi/180)]]
            matrixA = np.array(matrixA,dtype='float')
            matrixB = np.array(matrixB,dtype='float')
            res = np.dot(vect,matrixA)
            res = np.dot(res,matrixB)
            res[0] = res[0] - global_x[i] #+ global_x[i] #+ point[0] + global_x[i]
            res[1] = res[1] #+ point[1]
            res[2] = res[2] - global_z[i] #+ global_z[i] #+ point[2] + global_z[i]
            data[i] = res[0], res[1], res[2]
        return data

    def visualize(self):
        all_data_files = os.listdir(self.filename_load)
        point_cloud = np.loadtxt(self.filename_load + all_data_files[0], skiprows=1)
        point_cloud = self.prepare_data(point_cloud)
        del all_data_files[0]
        for data in all_data_files:
            try:
                point_cloud = np.concatenate((point_cloud, self.prepare_data(np.loadtxt(self.filename_load + data, skiprows=1))),axis=0)
            except:
                print('no data')
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(point_cloud[:, :3])

        o3d.visualization.draw_geometries([pcd])

    def main(self):
        rospy.init_node('sim4k_gazebo', anonymous=True)
        rospy.Subscriber('/camera/depth/points', PointCloud2, self.get_data)
        rospy.Subscriber("/odom", Odometry, self.odom)
        rospy.spin()


foo = MainClass()
user_input = input("1) Pobierz dane\n2) Zwizualizuj dane\n")
if user_input == "1":
    foo.main()
elif user_input == "2":
    foo.visualize()
