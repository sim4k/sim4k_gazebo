#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import Imu
from math import atan2, pi, degrees


class Yaw:
    def __init__(self):
        self.quaternion_orientation = [0, 0, 0, 0]
        self.yaw = 0
        rospy.Subscriber('/imu', Imu, self.imu_callback)

    def imu_callback(self, msg):
        self.quaternion_orientation = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]
        siny_cosp = 2 * (self.quaternion_orientation[3] * self.quaternion_orientation[2] +
                         self.quaternion_orientation[0] * self.quaternion_orientation[1])
        cosy_cosp = 1 - 2 * (self.quaternion_orientation[1] * self.quaternion_orientation[1] +
                             self.quaternion_orientation[2] * self.quaternion_orientation[2])
        self.yaw = degrees(atan2(siny_cosp, cosy_cosp)) + 180

    def get(self):
        return self.yaw
