#! /usr/bin/env python

# Inspired by github.com/avisingh599/mono-vo
# Modified to work on realtime image data retrieved from ROS topics.

import rospy
import numpy as np
from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
import message_filters
from cv_bridge import CvBridge, CvBridgeError
import cv2
import copy

class visual_odom(object):
    def __init__(self,focal_length,pp,lk_params):
        self.detector = cv2.FastFeatureDetector_create(threshold=25, nonmaxSuppression=True)
        self.lk_params = lk_params
        self.focal = focal_length
        self.pp = pp
        self.R = np.zeros(shape=(3, 3))
        self.t = np.zeros(shape=(3, 3))
        self.id = 0
        self.n_features = 0
        self.cur_pos = [0,0,0]
        self.prev_position = [0,0,0]
        self.true_coord =np.array([[0], [0], [0]])

        self.min_features = 1500

    def visual_odometery(self):
        if(self.id == 0):
            self.px_prev = self.detector.detect(self.cur_frame)
            self.px_prev = np.array([x.pt for x in self.px_prev], dtype=np.float32)
            self.id = 1
            return
        if(self.id == 1):
            self.kp2, st, err = cv2.calcOpticalFlowPyrLK(self.prev_frame, self.cur_frame, self.px_prev, None, **self.lk_params)
            st = st.reshape(st.shape[0])
            
            self.px_prev = self.px_prev[st == 1]
            self.px_cur = self.kp2[st == 1]

            E, _ = cv2.findEssentialMat(self.px_cur, self.px_prev, focal=self.focal, pp=self.pp, method=cv2.RANSAC, prob=0.999, threshold=1.0)
            _, self.R, self.t, _ = cv2.recoverPose(E, self.px_cur, self.px_prev, focal=self.focal, pp = self.pp)
            self.id = 2
            self.t = [[0],[0],[0]]
            self.px_prev = self.px_cur
            return
        if(self.id == 2):
            self.kp2, st, err = cv2.calcOpticalFlowPyrLK(self.prev_frame, self.cur_frame, self.px_prev, None, **self.lk_params)
            st = st.reshape(st.shape[0])
            
            self.px_prev = self.px_prev[st == 1]
            self.px_cur = self.kp2[st == 1]
            
            E, _ = cv2.findEssentialMat(self.px_cur, self.px_prev, focal=self.focal, pp=self.pp, method=cv2.RANSAC, prob=0.999, threshold=1.0)
            
            try:
                _, R, t, _ = cv2.recoverPose(E, self.px_cur, self.px_prev, focal=self.focal, pp = self.pp)
                absolute_scale = self.get_absolute_scale()
                # if(absolute_scale > 0.01):
                self.t = self.t + absolute_scale*self.R.dot(t)
                self.R = R.dot(self.R)
            except Exception as e:
                print(e)
                pass
            if(self.px_prev.shape[0] < self.min_features):
                self.px_cur = self.detector.detect(self.cur_frame)
                self.px_cur = np.array([x.pt for x in self.px_cur], dtype=np.float32)
            self.px_prev = self.px_cur
            return

    def vis_coordinates(self):
        diag = np.array([[-1, 0, 0],
                        [0, -1, 0],
                        [0, 0, -1]])
        adj_coord = np.matmul(diag, self.t)
        return adj_coord.flatten()
        # return [self.t[0],self.t[1],self.t[2]]

    def true_coordinates(self):
        return self.true_coord.flatten()

    def get_absolute_scale(self):
        x_prev = self.prev_position[0]
        y_prev = self.prev_position[1]
        z_prev = self.prev_position[2]
        x_cur = self.cur_pos[0]
        y_cur = self.cur_pos[1]
        z_cur = self.cur_pos[2]
        true_vect = np.array([[x_cur], [y_cur], [z_cur]])
        self.true_coord = true_vect
        return np.linalg.norm(np.array([[x_cur], [y_cur], [z_cur]]) - np.array([[x_prev], [y_prev], [z_prev]]))

class main_class:
    def __init__(self):
        self.current_frame = None
        self.past_frame = None
        self.bridge = CvBridge()
        self.window_name = "curr_frame"
        self.counter = 0
        self.cur_pos = [0,0,0]
        self.prev_pos = [0,0,0]
        self.frame_counter = 0
        self.pos_counter = 0
        # from rostopic echo \camera_info
        focal = 554.38271282264415
        # from rostopic echo \camera_info
        pp = (320.5, 240.5)
        # Parameters for lucas kanade optical flow
        lk_params = dict( winSize  = (21,21),criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 30, 0.01))
        self.vis_odom = visual_odom(focal, pp, lk_params)
        self.trajectory_img = np.zeros(shape=(800, 800, 3))

    def callback_camera(self,odom_data,data):
        self.prev_pos = copy.deepcopy(self.cur_pos)
        self.cur_pos[0] = odom_data.pose.pose.position.x
        self.cur_pos[1] = odom_data.pose.pose.position.y
        self.cur_pos[2] = odom_data.pose.pose.position.z
        self.pos_counter += 1
        try:
            self.past_frame = copy.deepcopy(self.current_frame)
            self.current_frame = self.bridge.imgmsg_to_cv2(data, "bgr8")
            self.current_frame = cv2.cvtColor(self.current_frame, cv2.COLOR_BGR2GRAY)
            self.frame_counter += 1
        except CvBridgeError as e:
            print(e)

        self.show_current_frame()
        print("frame: {}, pos: {}".format(self.frame_counter,self.pos_counter))
        self.odometry()

    def true_position(self,msg):
        self.prev_pos = copy.deepcopy(self.cur_pos)
        self.cur_pos[0] = msg.pose.pose.position.x
        self.cur_pos[1] = msg.pose.pose.position.y
        self.cur_pos[2] = msg.pose.pose.position.z
        self.pos_counter += 1

    def main(self):
        rospy.init_node('sim4k_gazebo', anonymous=True)
        odom_sub = message_filters.Subscriber("/odom", Odometry)
        img_sub = message_filters.Subscriber('/camera/rgb/image_raw', Image)
        ts = message_filters.ApproximateTimeSynchronizer([odom_sub, img_sub],  queue_size=5, slop=0.1)
        ts.registerCallback(self.callback_camera)
        rospy.spin()

    def odometry(self):
        self.vis_odom.cur_pos = self.cur_pos
        self.vis_odom.prev_position = self.prev_pos
        self.vis_odom.prev_frame = self.past_frame
        self.vis_odom.cur_frame = self.current_frame
        self.vis_odom.visual_odometery()
        try:
            self.draw_trajectory()
        except Exception as e:
            print(e)
            pass
    def draw_trajectory(self):
        vis_coord = self.vis_odom.vis_coordinates()
        print("ESTIMATED COORD")
        print(vis_coord)
        true_coord = self.vis_odom.true_coordinates()
        print("TRUE COORD")
        print(true_coord)
        vis_x, vis_y, vis_z = [x for x in vis_coord]
        true_x, true_y, true_z = [x for x in true_coord]

        # DRAW
        '''
        self.trajectory_img = cv2.circle(self.trajectory_img, (int(round(true_x,3)*100) + 200, int(round(true_y,3)*100) + 200), 1, list((0, 0, 255)), 4)
        self.trajectory_img = cv2.circle(self.trajectory_img, (int(round(vis_x,3)*100) + 200, int(round(vis_y,3)*100) + 200), 1, list((0, 255, 0)), 4) 
        cv2.putText(self.trajectory_img, 'Actual Position:', (140, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,255,255), 1)
        cv2.putText(self.trajectory_img, 'Red', (270, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 0, 255), 1)
        cv2.putText(self.trajectory_img, 'Estimated Odometry Position:', (30, 120), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,255,255), 1)
        cv2.putText(self.trajectory_img, 'Green', (270, 120), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 255, 0), 1)
        cv2.imshow('trajectory', self.trajectory_img)
        '''
        # To file
        print("write")
        with open("vis_data.txt","a") as f:
            f.writelines("{} {}\n".format(vis_x,vis_y))

    def show_current_frame(self):
        cv2.imshow(self.window_name, self.current_frame)  
        cv2.waitKey(1)

foo = main_class()
foo.main()
cv2.destroyAllWindows()
