#!/usr/bin/env python3
import rospy
from geometry_msgs.msg import Twist


class Movement:
	def __init__(self):
		self.velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
		self.vel_msg = Twist()
		self.vel_msg.linear.x = 0
		self.vel_msg.linear.y = 0
		self.vel_msg.linear.z = 0
		self.vel_msg.angular.x = 0
		self.vel_msg.angular.y = 0
		self.vel_msg.angular.z = 0
		self.is_forward = False
		self.is_turn = False

	def move_forward(self, vel_forward):		
		self.vel_msg.linear.x = vel_forward
		self.vel_msg.angular.z = 0
		self.velocity_publisher.publish(self.vel_msg)
		self.is_forward = True
		self.is_turn = False

	def turn(self, twist):
		self.vel_msg.linear.x = 0
		self.vel_msg.angular.z = twist
		self.velocity_publisher.publish(self.vel_msg)
		self.is_forward = False
		self.is_turn = True

	def stop(self):
		self.vel_msg.linear.x = 0
		self.vel_msg.angular.z = 0
		self.velocity_publisher.publish(self.vel_msg)
		self.is_forward = False
		self.is_turn = False





