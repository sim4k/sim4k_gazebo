#!/usr/bin/env python3

import rospy
import numpy as np
from geometry_msgs.msg import Twist, Point
from math import atan2
from nav_msgs.msg import Odometry
from scipy.spatial.transform import Rotation as R
import random


class RobotOdometry:
    """
    Class for robot odometry, contains robot position and orientation
    """
    def __init__(self):
        self._x = 0
        self._y = 0
        self._theta = 0
        self.sub = rospy.Subscriber("/odom", Odometry, self.__get_odom_callback)

    def __get_odom_callback(self, msg):
        self._x = msg.pose.pose.position.x
        self._y = msg.pose.pose.position.y

        rot_q = msg.pose.pose.orientation
        rot = R.from_quat([rot_q.x, rot_q.y, rot_q.z, rot_q.w])
        (roll, pitch, self._theta) = rot.as_euler('xyz', degrees=True)

    @staticmethod
    def get_shortest_angle(ang1, ang2):
        pi2 = 6.28
        angle_diff = (ang1 - ang2) % pi2
        if angle_diff < 0.0 and abs(angle_diff) > (pi2 + angle_diff):
            angle_diff = pi2 + angle_diff
        elif angle_diff > abs(angle_diff - pi2):
            angle_diff = angle_diff - pi2
        return angle_diff


# rospy.init_node('sim4k_gazebo', anonymous=True)
#
# sub = rospy.Subscriber("/odom", Odometry, new_odom)
# pub = rospy.Publisher("/cmd_vel", Twist, queue_size = 1)
#
# vel_msg = Twist()
# r = rospy.Rate(1000)
#
# trace = np.array([[0, 0], [0, 0.65], [0.65, 0.65], [0.65, 0]])
# trace_trgt = 0

# while not rospy.is_shutdown():
#     # print("Position: {},{}".format(x, y))
#     # print("Rotation {}".format(theta))
#     if np.linalg.norm(np.array([x, y])-trace[trace_trgt]) < 0.1:
#         trace_trgt += 1
#         if trace_trgt == len(trace):
#             trace_trgt = 0
#
#     print(np.rad2deg(atan2(trace[trace_trgt][1]-y, trace[trace_trgt][0]-x)))
#     print(theta)
#     if abs(atan2(trace[trace_trgt][1]-y, trace[trace_trgt][0]-x) - np.radians(theta)) > 0.3:
#         vel_msg.linear.x = 0.0
#         vel_msg.angular.z = shortest_angle(atan2(trace[trace_trgt][1]-y, trace[trace_trgt][0]-x), np.radians(theta))*2
#     else:
#         vel_msg.linear.x = 0.5
#         vel_msg.angular.z = 0.0
#     pub.publish(vel_msg)
#     r.sleep()