#!/usr/bin/env python3
import rospy
from odometry import RobotOdometry
from steering import Movement
from distance import Distance
import numpy as np
import time
from geometry_msgs.msg import Twist, Point
from math import atan2
from nav_msgs.msg import Odometry
from scipy.spatial.transform import Rotation as R
import yaml
# TODO: this is main script for robot movement, here we start
#  other classes like odometry, steering or class for measuring distance from obstacle


class AutonomousControl:
	def __init__(self):
		rospy.init_node('sim4k_gazebo', anonymous=True)
		self.odometry = RobotOdometry()
		print("odoemtry check")
		self.movement = Movement()
		print("movement check")
		self.r = rospy.Rate(100)
		self.distance = Distance()
		print("distance check")
		self.turn_time = time.time()
		self.turn_side = 0
		print("turn variables check")
		self.movement_algorithm_loop()
	
	def get_x(self):
		return self.odometry._x
	
	def get_y(self):
		return self.odometry._y
	
	def get_angle(self):
		return self.odometry._theta
	
	def movement_trace_algorithm_loop(self):
		trace = np.array([[0, 0], [0, 0.65], [0.65, 0.65], [0.65, 0]])
		trace_trgt = 0
		
		while not rospy.is_shutdown():
			x = self.get_x()
			y = self.get_y()
			print(x)
			print(y)
			print(self.distance.all_data)
			angle = self.get_angle()
			if np.linalg.norm(np.array([x, y])-trace[trace_trgt]) < 0.1:
				trace_trgt += 1
				if trace_trgt == len(trace):
					trace_trgt = 0
	
			print(np.rad2deg(atan2(trace[trace_trgt][1]-y, trace[trace_trgt][0]-x)))
			print(angle)
			if abs(atan2(trace[trace_trgt][1]-y, trace[trace_trgt][0]-x) - np.radians(angle)) > 0.3:
				self.movement.turn(self.odometry.get_shortest_angle(
					atan2(trace[trace_trgt][1]-y,
					trace[trace_trgt][0]-x),
					np.radians(angle))*2)
			else:
				self.movement.move_forward(0.5)
			self.r.sleep()
		print("END")

	def movement_algorithm_loop(self):
		import os
		while not rospy.is_shutdown():
			x = self.get_x()
			y = self.get_y()
			angle = self.get_angle()

			with open('src/sim4k_gazebo/src/rosbot_description/config.yaml') as file:
				config = yaml.load(file)

			distance = self.distance.all_data

			if self.turn_time > time.time():
				print('turning')
				self.movement.turn(config['angular_speed'] * self.turn_side)
			else:
				if distance[0] < 0.4:
					print('collision_detected')
					print(distance[0])
					self.movement.move_forward(0)
					self.turn_side = 1 if distance[1] > distance[-1] else -1
					self.turn_time = time.time() + np.random.rand() * 10 + 10
				else:
					print('driving forward')
					self.movement.move_forward(config['linear_speed'])

			self.r.sleep()
		print("END")


control = AutonomousControl()
