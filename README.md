# Sim4k 
## Budowa robota czterokołowego w Gazeboo i estymacja jego położenia wraz z tworzeniem mapy 3d w oparciu fuzję danych z IMU, kamery RGBD (na robocie) oraz lidaru

# Wymagania
- System Operacyjny Ubuntu 18.04 LTS
- Robot Operating System (ROS) w wersji Melodic


# Instrukcja obsługi skryptu inicjalizującego środowisko i funkcjonalności projektu

Niniejszy opis stanowi skróconą wersję opisu zawartego w issue #13.

Aby w uproszoczony sposób zainicjalizować projekt SIM4K 
bez potrzeby ręcznego git clone'owania repozytorium należy:

- skopiować zawartość pliku `sim4k.sh` pod tą samą nazwą do docelowej lokalizacji na komputerze osobistym
- Uruchomić konsolę w miejscu skopiowania skryptu, dodać wykonywalność pliku poprzez `chmod +x sim4k.sh`
- Uruchomić skrypt poprzez `bash sim4k.sh`. Terminal powinien wyglądać w następujący sposób:

![mainmenu_image](/screenshots/mainmenu.png)
- Aby zainicjalizować środowisko należy jedynie nacisnąć przycisk `1`. Skrypt zabezpiecza użytkownika przed duplikacją środowiska.


## Mapowanie 3D

Pełny opis rozwiązania został zawarty w issue #6. Aby uruchomić mapping przy użyciu `sim4k.sh` należy:
 - uruchomić dwa terminale, w każdej wykonać komendę `bash sim4k.sh`
 - w 1 terminalu wybrać 2 (launch simulation) i poczekać, aż gazebo się uruchomi
 - w 2 terminalu wybrać 3 (launch mapping) 
 - opcjonalnie w 3 terminalu wybrać 4 (ręczne sterowanie robotem)
 - Po pewnym czasie nastąpi komunikat:

 ![3dmap_image](screenshots/3dmap.png)

 
 - należy wybrać opcję 1) - pliki z pomiarami zostaną zapisane
 - po zapisie pomiarów należy kombinacją ctrl+C wrócić do poprzedniego menu
 - po powrocie do poprzedniego menu należy wybrać opcję 2) - wizualizację danych.
 
 ## Hector SLAM
 
 Pełny opis rozwiązania został zawarty w issue #11.
 Aby uruchomić rozwiązanie należy:
 - w trzech terminalach wywołać polecenie `bash sim4k.sh`
 - w każdym terminalu wybrać opcję 6) (enter hector SLAM menu)
 - w 1 terminalu wybrać opcję 1) (uruchomienie Gazebo)
 - w 2 terminalu wybrać opcję 2) (uruchomienie Rviz)
 - w 3 terminalu wybrać opcję 3) (sterowanie robotem)
 
 ## Autonomiczne sterowanie robotem
Pełny opis rozwiązania został zawarty w issue #3 oraz #9.
Narzędzia wykorzystane do tej implementacji (pobieranie odległości) są opisane w issue #8.

Aby uruchomić rozwiązanie należy:
 - uruchomić dwa terminale, w każdej wykonać komendę `bash sim4k.sh`
 - w 1 terminalu wybrać 2 (launch simulation) i poczekać, aż gazebo się uruchomi
 - w 2 terminalu wybrać 5 (Autonomous control)

# Ręczna inicjalizacja środowiska i uruchomienie symulacji
1. W lokalizacji ~ ( home, ale może być dowolnie wybrana ) stworzyć folder ros_workspace i w nim folder src:   
    ```
    cd ~
    mkdir ros_workspace
    mkdir ros_workspace/src
    cd ~/ros_workspace/src
    catkin_init_workspace
    cd ~/ros_workspace
    catkin_make
    ```
2. Sklonować repozytorium do src:  
    ```
    cd ~/ros_workspace/src
    git clone https://gitlab.com/sim4k/sim4k_gazebo.git
    ```
3. Przejść na odpowiedni branch z plikami
    ```
    cd ~/ros_workspace/src/sim4k_gazebo
    git checkout nazwabrancha
    ```
4. 
    ```
    cd ~/ros_workspace
    rosdep install --from-paths src --ignore-src -r -y
    ```
    ```
    cd ~/ros_workspace
    catkin_make
    ```
5. Wczytać zmienne systemowe:  
    ```
    cd ~/ros_workspace
    source devel/setup.sh
    ```
6. Uruchomić symulację Gazebo:  
    ```
    cd ~/ros_workspace
    roslaunch rosbot_description rosbot_rviz_gmapping.launch
    ```
7. By uruchomić sterowanie klawiaturą: otworzyć nowy terminal i wykonać krok 5. i wykonać komendę:
    ```
    cd ~/ros_workspace
    roslaunch rosbot_navigation rosbot_teleop.launch
    ```

## GitLab Workflow dla deweloperów

### Ogólne zasady
1. Nazywać commity krótko i treściwie - rozwinięcie w komentarzu do commita
2. Jeden commit - jedna funkcjonalność, żeby w razie konieczności powrotu do jakiegoś punktu 
cofać się o tylko jeden krok.
3. Nazwa branch'a  - numer issue i krótkie rozwinięcie (np. 1_mapy, 2_model_robot itp )
4. Korzystać z git'a w konsoli. Gitkrakeny i inne graficzne narzędzia do gita - nie polecam.

## Procedura


### tworzenie nowego branch'a
1. Jesteśmy na branchu master
2. Pobieramy najnowsze zmiany poprzez:
    ```sh
    $ git pull origin
    ```
3. tworzymy nowego brancha i przełączamy się na niego komendą:
    ```sh
    $ git checkout -b NAZWA_BRANCHA
    ```

### dodawanie/odejmowanie plików do commitu i commitowanie
Jesteśmy na pobocznym branchu, na którym tworzymy swój feature.

1. Dodajemy/usuwamy/edytujemy pliki projektu
2. Dodawanie wszystkich plików do commita (-a lub .)
    ```sh
    $ git add -A
    ```
    lub 
    ```
    $ git add .
    ```
3. Odejmowanie plików do commita jeśli dodacie za dużo, git podpowiada wam jak możecie odznaczyć pliki
    ```sh
   Changes to be committed:
   (use "git reset HEAD <file>..." to unstage)
   $ git reset HEAD 'nazwa_pliku lub sciezka do pliku np. tests/test3.py'
    ```
4. Sprawdzanie statusu do commita
    ```sh
    $ git status
    ```
    Wówczas dzięki kolorom w konsoli i sugestiom gita widzimy co będzie dodawane, a co nie do commita
5. Commitujemy dodane pliki
    ```sh
    $ git commit -m 'NAZWA COMMITU'
    ```
6. Jeśli chcemy, by commity były widoczne w globalnym repo (bo git commit działa najpierw na lokalnym), to:
    ```sh
    $ git push
    ``` 

### Mergowanie do mastera(maina) i tworzenie PR'ów (Pull Requestów)
Jesteśmy na pobocznym branchu, na którym tworzymy swój feature.
1. Przechodzimy na branch master
    ```sh
    $ git checkout master
    ```
2. Pobieramy ewentualne najnowsze zmiany z repozytorium zdalnego
    ```sh
    $ git pull origin
    ```
3. Przechodzimy na nasz branch i robimy rebase
    ```sh
    $ git checkout NAZWA_BRANCHA
    $ git rebase master
    ```
4. Pushujemy pliki
    ```sh
    $ git push origin NAZWA_BRANCHA
    ```
10. Wyświetli nam się w konsoli link w który powinniśmy wejść przez przeglądarke
11. W polu description dobrze opisać co dodajemy w danym merge requescie
12. W polu assignee wybieramy osobe z projektu która ma ocenić/zrecenzować nasz kod (lidera 1/2 i jeszcze jedną osobę)
13. Dajemy submit merge request
14. Na konwersacji na fb wysłać link do merge requesta i oznaczyli osobe do sprawdzenia.

### Poprawianie zmian po komentarzach w review merge requesta
1. Jesteśmy na lokalnym branchu z którego pushowalismy nasze zmiany do merge requesta.
2. Implementujemy zaproponowane zmiany.
3. Gdy skończymy należy dodać zmiany do repozytorium komendą:
    ```sh
    $ git add -A lub git add .
    ```
4. Następnie dodajemy wszelkie zmienione rzeczy do poprzedniego commita
   ```sh
   $ git commit --amend
   ```
5. Otworzy nam się edytor tekstowy, jeśli nie chcemy zmieniać nazwy commita to należy wcisnąć następującą sekwencję: ESCAPE, :wq, ENTER
6. Nastepnie należy wykonać rebase z masterem, w tym celu wykonujemy:
    ```sh
    $ git checkout master
    $ git pull origin
    $ git checkout NAZWA_BRANCHA
    $ git rebase master
    ```
7. Następnie pushujemy nasze zmiany.

   UWAGA!!! Nazwa brancha na który pushujemy musi byc dokładnie taka sama jak nazwa brancha na który wcześniej pushowalismy(można to sprawdzić w swoim merge requescie)
   ```sh
   $ git push --force origin NAZWA_PIERWOTNEGO_BRANCHA
   ```
