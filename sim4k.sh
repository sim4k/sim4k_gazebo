#!/bin/bash

setworkspace() {
	echo
	echo "Setting your workspace..."
	echo "It may take few minutes."
	echo

	#Prepare System
	if test ! -d /opt/ros/melodic; then
	  echo "This script assumes ROS melodic to be installed"
	  echo "The directory /opt/ros/melodic was not found"
	  exit 1
	fi

	echo
	echo Initializing catkin worskspace
	echo

	SUBDIR=~/sim4k_workspace
	echo "This script puts all code into '$SUBDIR'."

	mkdir $SUBDIR
	mkdir $SUBDIR/src

	cd $SUBDIR/src
	catkin_init_workspace

	cd ..
	catkin_make

	cd $SUBDIR/src
	git clone https://gitlab.com/sim4k/sim4k_gazebo.git

	cd $SUBDIR
	rosdep install --from-paths src --ignore-src -r -y
	
	cd ~/sim4k_workspace/src/sim4k_gazebo/src/rosbot_description
	chmod +x autonomous_control.py
	chmod +x map3d.py

	cd $SUBDIR
	catkin_make
}

launchsim() {
	echo
	echo
	echo "Launching gazebo simulation with rviz"
	cd ~/sim4k_workspace
	source devel/setup.bash
	roslaunch rosbot_description rosbot_rviz_gmapping.launch
}

launchnavi() {
	echo
	echo
	echo "Launching manual control"
	cd ~/sim4k_workspace
	source devel/setup.bash
	roslaunch rosbot_navigation rosbot_teleop.launch
}

launchautonomous() {
	echo
	echo
	echo "Launching autonomous control script"
	cd ~/sim4k_workspace
	source devel/setup.bash
	rosrun rosbot_description autonomous_control.py
}

hectormenu() {
	echo
	echo "This is Hector Slam menu"
	echo
	echo "Press 1 to launch simulation"
	echo "Press 2 to launch hectorslam"
	echo "Press 3 to activate manual control"
	echo "Press x to return to first menu"
	read  -n 1 -p "Input Selection:" hectorinput

	if [ "$hectorinput" = "1" ]; then
	    launchhector1
	    hectormenu
        elif [ "$hectorinput" = "2" ]; then
            launchhector2
	    hectormenu    
        elif [ "$hectorinput" = "3" ]; then
            launchnavi
	    hectormenu
	elif [ "$hectorinput" = "x" ]; then
	    echo
	    echo
	    echo "Returning to main menu"
	    echo "Press any key to continue..."
    	    read -n 1
	    clear
	    menu
	else
	    echo
            echo "You have entered an invallid selection!"
            echo "Please try again!"
            echo 
            echo "Press any key to continue..."
            read -n 1
            clear
            hectormenu
        fi
}

launchhector1() {
	echo
	echo
	echo "Launching gazebo simulation"
	cd ~/sim4k_workspace
	source devel/setup.bash
	roslaunch rosbot_description rosbot.launch
}

launchhector2() {
	echo
	echo
	echo "Launching Hector SLAM"
	cd ~/sim4k_workspace
	source devel/setup.bash
	roslaunch rosbot_description hector_slam.launch
}

launchmap3d() {
	echo
	echo 
	echo "Launching 3d map"
	cd ~/sim4k_workspace
	source devel/setup.bash
	rosrun rosbot_description map3d.py
}

menu() {
	echo
	echo
	echo "This is main menu"
	echo
	echo
	echo "Press 1 to install"
	echo "Press 2 to launch simulation"
	echo "Press 3 to launch mapping"
	echo "Press 4 to activate manual control"
	echo "Press 5 to activate autonomous control"
	echo "Press 6 to enter hectorslam menu"
	echo "Press x to exit program"
	read  -n 1 -p "Input Selection:" menuinput
	echo

	if [ "$menuinput" = "1" ]; then
	    if test -d ~/sim4k_workspace; then
		echo
		echo "Workspace already exists!"
		echo
		echo "Press any key to continue..."
            	read -n 1
		clear
		menu
	    else
		setworkspace
		menu	
	    fi
	elif [ "$menuinput" = "2" ]; then
            launchsim
	    menu
        elif [ "$menuinput" = "3" ]; then
            launchmap3d
	    menu
        elif [ "$menuinput" = "4" ]; then
            launchnavi
	    menu
	elif [ "$menuinput" = "5" ]; then
            launchautonomous
	    menu
	elif [ "$menuinput" = "6" ]; then
	    clear
            hectormenu
        elif [ "$menuinput" = "x" ];then
	    echo
	    echo "You have closed SIM4k program."
	    echo "Press any key to continue..."
            read -n 1		
            exit 1
        else
	    echo
            echo "You have entered an invallid selection!"
            echo "Please try again!"
            echo 
            echo "Press any key to continue..."
            read -n 1
            clear
            menu
        fi
}

hellofun() {
	echo
	echo "Welcome to the SIM4k Gazebo Project"
	echo "Press any key to continue..."
	echo
	read -n 1
	clear
	menu
}

hellofun
menu
